# devops-practice
Project for educational purposes, created while studing DevOps

## Setup

1. Run `docker-compose up`
2. Open 0.0.0.0/api/

## API actions

API links references

### api/auth/
#### POST

Returns API token for authentication.

JSON Data:
* username
* password

### api/users/
#### GET

Returns a list of users.

#### POST

Creates a new user.

JSON data:
* username
* email
* password

### api/users/(id)
#### GET

Returns a single user representation.


#### POST

Uset data update endpoint. Requires owner permissions.

JSON data:
* username
* email
* password

#### DELETE

Deletes a user data. Requires admin permissions.


### api/articles/
#### GET

Returns a list of articles.

#### POST

Creates a new article. Requires user authorization.

JSON data:
* title
* body

### api/articles/(id)
#### GET

Return a signel article representation.

#### POST

Updates article data. Requires author permissions.

JSON data:
* title
* body

### api/articles/(id)/comments
#### GET

Returns a list of article's comments.

#### POST

Creates a new comment.

JSON data:
* body

### api/comments/
#### GET

Returns a list of all comments

#### POST

Creates a new comment. Requires user authorization.

JSON data:
* body
* article (id)

### api/comments/(id)
#### GET

Returns a single comment representation.

#### POST

Updates comment data. Requires owner permission.

JSON data:
* body
* article (id)

