from rest_framework import permissions


class IsAccountOwnerOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        if view.action == 'create' and request.user.is_authenticated:
            return False

        if view.action in ('list', 'create'):
            return True

        return False

    def has_object_permission(self, request, view, object):

        if view.action == 'retrieve':
            return True

        if not request.user.is_authenticated:
            return False

        is_account_owner = object.id == request.user.id

        if view.action in ('update', 'partial_update') and is_account_owner:
            return True

        return False


class IsAuthorOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):

        if not request.user.is_authenticated:
            return False

        if request.method in permissions.SAFE_METHODS:
            return True

        if view.action in ('list', 'create'):
            return True

        return False

    def has_object_permission(self, request, view, object):

        if not request.user.is_authenticated:
            return False

        if request.method in permissions.SAFE_METHODS:
            return True

        if view.action == 'retrieve':
            return True

        is_author = object.author.id == request.user.id

        if view.action in ('update', 'partial_update') and is_author:
            return True

        return False
