from django.test import TestCase
from django.contrib.auth.models import User

from blog.models import Article


class TestArticle(TestCase):

    def test_article_creation(self):
        self.author = User.objects.create_user(
            username='Test',
            password='123',
            email='test@example.com'
        )

        self.article = Article.objects.create(
            title='A simple title',
            body='Some body text',
            author=self.author
        )

        self.assertIsNotNone(self.article.id)
        self.assertEquals(self.article.author.id, self.author.id)
