from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from blog.models import Article, Comment
from blog.serializers import (
    UserCreateSerializer, UserSerializer, ArticleSerializer, CommentSerializer
)
from blog.permission import IsAccountOwnerOrReadOnly, IsAuthorOrReadOnly


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = (IsAccountOwnerOrReadOnly,)

    def get_serializer_class(self):
        if self.action == 'create':
            return UserCreateSerializer

        return UserSerializer

    def create(self, request):
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_permissions(self):
        if self.action == 'comments':
            return (IsAuthenticated(), )
        return (IsAuthorOrReadOnly(), )

    @action(detail=True, methods=('get', 'post'))
    def comments(self, request, pk=None):
        article = get_object_or_404(Article, pk=pk)

        if request.method == 'GET':
            comments = article.comments.all()

            serializer = CommentSerializer(
                comments, many=True
            )

            return Response(serializer.data)

        elif request.method == 'POST':

            request.data['article'] = article.id
            serializer = CommentSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(author=request.user)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)

            return Response(serializer.errors)


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (IsAuthorOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
