from blog import views

from django.urls import path, include

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter


app_name = 'blog'


router = DefaultRouter()
router.register('users', views.UserViewSet, basename='users')
router.register('articles', views.ArticleViewSet, basename='articles')
router.register('comments', views.CommentViewSet, basename='comments')


urlpatterns = [
    path('api/auth/', obtain_auth_token, name='api-auth'),
    path('api/', include(router.urls))
]
